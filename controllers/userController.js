const User = require("../models/userModel.js");

module.exports = {

    userCreate: (request, response) => {
        User.create({ userId: request.body.userId, userName: request.body.userName, userAge: request.body.userAge }, function (err, doc) {
            if (err) return console.log(err);
            console.log("Сохранен объект user" + doc);
            response.send(doc);
        });
    },

    getUserInfo: (request, response) => {
        let id = request.params.id
        User.find({userId : id}, function (err, docs) { 
            if (err) return console.log(err);
            console.log(docs);
            response.send("Get info about" + docs);
        });

    },

    deleteUser: (request, response) => {
        let id = request.params.id;
        User.findOneAndDelete({userId : id}, function (err, doc) {
            if (err) return console.log(err);
            console.log(`Удален пользователь ${doc}`);
            response.send(`Удален пользователь ${doc}`);
        })
    },

    updateUser: (request, response) => {
        let id = request.params.id;
        User.findOneAndUpdate({userId : id}, {userId: request.body.userId, userName: request.body.userName, userAge: 25}, {new: true}, function(err, user){
            if(err) return console.log(err);
            console.log("Обновленный объект" + user);
            response.send("Change info about user" + user);
        });
    }
};

