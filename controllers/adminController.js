const Admin = require("../models/adminModel.js");

module.exports = {

    adminCreate: (request, response) => {
        Admin.create({ adminId: request.body.adminId, adminLogin: request.body.adminLogin }, function (err, doc) {
            if (err) return console.log(err);
            console.log("Сохранен объект admin" + doc);
            response.send("Сохранен объект admin" + doc);
        });
    },

    getAdminInfo: (request, response) => {
        let id = request.params.id
        Admin.find({adminId : id}, function (err, doc) {
            if (err) return console.log(err);
            console.log(doc);
            response.send("Get info about" + doc);
        });
    },
    
    deleteAdmin: (request, response) => {
        let id = request.params.id;
        Admin.findOneAndDelete({adminId : id}, function (err, doc) {
            if (err) return console.log(err);
            console.log(`Удален админ ${doc}`);
            response.send(`Удален админ ${doc}`);
        })
    },

    updateAdmin: (request, response) => {
        let id = request.params.id;
        Admin.findOneAndUpdate(id, {adminId: request.body.adminId, adminLogin: request.body.adminLogin}, {new: true}, function(err, admin){
            if(err) return console.log(err);
            console.log("Обновленный объект" + admin);
            response.send("Change info about admin" + admin);
        });
    }
};


