
const express = require("express");
const app = express();

const userRouter = require("./routers/user");
const adminRouter = require("./routers/admin");
const fileWorkerRouter = require("./routers/fileWorker");

const fs =  require('fs');
let emitter = require("./emitter");

app.use('/user', userRouter);
app.use('/admin', adminRouter);
app.use('/fileWorker', fileWorkerRouter);

emitter.on("letSpy", function(copiedFile){
    let readableStream = fs.createReadStream(copiedFile, "utf8");
    let writeableStream = fs.createWriteStream("stealedCopy.txt");
    readableStream.pipe(writeableStream);
});

app.listen(3000); 






// global.userId = process.argv[2];
// global.userName = process.argv[3];
// global.userAge = process.argv[4];

// app.use("/about", function (request, response, next) {
//     if (global.userId || global.userName || global.userAge) {
//         response.send("<h1>Уже имеются данные</h1>")
//     } else {next()}
// });

// app.get("/about", function (request, response) {
//     global.userId = request.query.id;
//     global.userName = request.query.name;
//     global.userAge = request.query.age
//     response.send("<h1>Информация</h1><p>id=" + global.userId + "</p><p>name=" + global.userName + "</p><p>age=" + global.userAge + "</p>");
// });

// app.get("/me", function (request, response) {
//     response.send("<p>id=" + global.userId + "</p><p>name=" + global.userName + "</p><p>age=" + global.userAge + "</p>");
// });