const mongoose = require("mongoose");
const Schema = mongoose.Schema;
mongoose.connect("mongodb://localhost:27017/usersdb", { useUnifiedTopology: true, useNewUrlParser: true, useFindAndModify: false  } );  // { useUnifiedTopology: true, useNewUrlParser: true } <<= прочитать про доп параметры

const userScheme = new Schema({
    userId: { type: Number,
        required: true,
        minlength :4,
    },
    userName: { type: String,
        min: 3,
        max: 20,
        default: "NoName"
    },
    userAge: {
        type: Number,
        required: true,
        min: 18,
        max: 100,
        default: 18
    }   
},
{versionKey: false }
);

const User = mongoose.model("User", userScheme);
module.exports = User;
