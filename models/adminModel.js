const mongoose = require("mongoose");
const Schema = mongoose.Schema;
mongoose.connect("mongodb://localhost:27017/usersdb", { useUnifiedTopology: true, useNewUrlParser: true, useFindAndModify: false });  // { useUnifiedTopology: true, useNewUrlParser: true } <<= прочитать про доп параметры

const adminScheme = new Schema({
    adminId: { type: Number,
        required: true,
        minlength :4,
    },
    adminLogin: { type: String,
        required: true,
        min: 18,
        max: 100,
        default: 18
    }
},
);

const Admin = mongoose.model("Admin", adminScheme);
module.exports = Admin;