const mongoose = require("mongoose");
const Schema = mongoose.Schema;
mongoose.connect("mongodb://localhost:27017/usersdb", { useUnifiedTopology: true, useNewUrlParser: true, useFindAndModify: false  } );  // { useUnifiedTopology: true, useNewUrlParser: true } <<= прочитать про доп параметры

const roomScheme = new Schema({
    number: { type: Number,
        required: true,
        max: 20,
    },
    users: { type: String,

        max: 20,
        default: "NoName"
    },
    admin: {
        type: Number,
        required: true,
        min: 18,
        max: 100,
        default: 18
    }   
},
{versionKey: false }
);

const Room = mongoose.model("Room", roomScheme);
module.exports = Room;
