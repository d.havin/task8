const express = require("express");
const adminRouter = express.Router();
const adminParser = express.json();
const adminController = require("../controllers/adminController");

// const bodyParser = require("body-parser");
// const adminParser = bodyParser1.urlencoded({extended: true});

adminRouter.post("/create", adminParser , adminController.adminCreate);
adminRouter.get("/:id", adminParser, adminController.getAdminInfo);
adminRouter.delete("/:id", adminParser, adminController.deleteAdmin);
adminRouter.put("/:id", adminParser, adminController.updateAdmin);

module.exports = adminRouter;

